import java.io.File;  
import java.io.IOException;  
  
public class CreateFileExample {  
    public static void main(String[] args) {  
        try {  
            // 指定要创建文件的路径和名称  
            File file = new File("C:/example/file.txt");  
  
            // 如果文件不存在，则创建文件  
            if (file.createNewFile()) {  
                System.out.println("File created successfully!");  
            } else {  
                System.out.println("File already exists.");  
            }  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }  
}